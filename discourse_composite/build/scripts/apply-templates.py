#!/usr/bin/env python3
# coding=utf-8

import pathlib
import os
import sys

import subprocess

PUPS_LOCATION = "/pups/bin/pups"

TEMPLATES_ROOT = "/discourse-composite/"

if __name__ == "__main__":

    templates = sys.argv[1:]

    root = pathlib.Path(TEMPLATES_ROOT)

    for template in templates:
        subprocess.check_call([PUPS_LOCATION, str(root / template)])
