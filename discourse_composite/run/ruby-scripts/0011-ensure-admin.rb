
def ensure_admin
  email = ENV['COMPOSE_DISCOURSE_ADMIN_EMAIL']
  password = ENV['COMPOSE_DISCOURSE_ADMIN_PASSWORD']
  username = ENV['COMPOSE_DISCOURSE_ADMIN_USERNAME']

  raise "Missing COMPOSE_DISCOURSE_ADMIN_EMAIL" unless email
  raise "Missing COMPOSE_DISCOURSE_ADMIN_PASSWORD" unless password
  raise "Missing COMPOSE_DISCOURSE_ADMIN_USERNAME" unless username

  admin = User.find_by_email(email)

  if not admin
    admin = User.new
    admin.email = email
    admin.username = username
  end

  if not admin.confirm_password?(password)
    admin.password = password
    raise "Couldn't save admin" unless admin.save!()
  end

  admin.active = true
  raise "Couldn't save admin" unless admin.save!()
  admin.grant_admin!
  admin.change_trust_level!(4)
  admin.email_tokens.update_all  confirmed: true
  admin.activate
  raise "Couldn't save admin" unless admin.save!()

end

if ENV['COMPOSE_DISCOURSE_ADMIN_EMAIL']
  begin
    ensure_admin();
  rescue
    abort("Exception in enforce sso`")
  end
end
