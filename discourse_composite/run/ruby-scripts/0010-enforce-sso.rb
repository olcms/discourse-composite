
def enforce_sso
  sso_secret = ENV["COMPOSE_DISCOURSE_SSO_SECRET"]
  raise "Missing COMPOSE_DISCOURSE_SSO_SECRET" unless sso_secret

  SiteSetting.sso_secret = sso_secret

  sso_redirect = ENV["COMPOSE_DISCOURSE_SSO_PROVIDER_URL"]
  raise "Missing COMPOSE_DISCOURSE_SSO_PROVIDER_URL" unless sso_redirect

  SiteSetting.sso_url = sso_redirect.chomp('/')
  SiteSetting.email_editable = false

  SiteSetting.enable_sso = true
  SiteSetting.invite_only = true

  SiteSetting.sso_overrides_email = true
  SiteSetting.sso_overrides_username = true
  SiteSetting.sso_allows_all_return_paths = true

end

if ENV['COMPOSE_DISCOURSE_SSO_SECRET']
  begin
    enforce_sso()
  rescue
    abort("Exception in enforce sso`")
  end
end
