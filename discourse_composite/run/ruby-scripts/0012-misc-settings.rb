

def apply_boolean_setting(key, setting)

  if ENV[key].nil?
    return
  end

  yes_vals = Set.new ['t', 'true', 'yes', 'y', '1']
  no_vals = Set.new ['f', 'false', 'no', 'n', '0']

  value = ENV[key].downcase

  if yes_vals.include? value
    SiteSetting.send("#{setting}=", true)
    return;
  end

  if no_vals.include? value
    SiteSetting.send("#{setting}=", false)
    return;
  end

  raise "Unknown value for #{setting}. Value is #{value}"

end

def apply_string_setting(key, setting)

    if ENV[key].nil?
      return
    end

    value = ENV[key]

    SiteSetting.send("#{setting}=", value)

end

def misc_settings

  apply_boolean_setting('COMPOSE_DISCOURSE_INVITE_ONLY', 'invite_only');
  apply_boolean_setting('COMPOSE_DISCOURSE_LOGIN_REQUIRED', 'login_required');
  apply_boolean_setting('COMPOSE_DISCOURSE_BOOTSTRAP_MODE', 'bootstrap_mode_enabled');

  apply_string_setting('COMPOSE_DISCOURSE_TITLE', 'title');
  apply_string_setting('COMPOSE_DISCOURSE_SITE_DESCRIPTION', 'site_description');

  apply_string_setting('COMPOSE_DISCOURSE_CONTACT_EMAIL', 'contact_email');
  apply_string_setting('COMPOSE_DISCOURSE_CONTACT_URL', 'contact_url');

  apply_string_setting('COMPOSE_DISCOURSE_COMPANY_SHORT_NAME', 'company_short_name');
  apply_string_setting('COMPOSE_DISCOURSE_COMPANY_FULL_NAME', 'company_full_name');
  apply_string_setting('COMPOSE_DISCOURSE_COMPANY_DOMAIN', 'company_domain');

end

begin
  misc_settings();
rescue
  abort("Exception misc settings")
end
