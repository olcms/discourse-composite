#!/usr/bin/env bash

while ! pg_isready -t 3 -d ${DISCOURSE_DB_NAME:-discourse} -h ${DISCOURSE_DB_HOST} -U ${DISCOURSE_DB_USERNAME:-discourse} -p ${DISCOURSE_DB_PORT:-5432}; do
    echo "Waiting for db"
    sleep 1
done
