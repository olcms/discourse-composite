#!/usr/bin/python3
# coding=utf-8

import pathlib
import subprocess
import sys

scripts = pathlib.Path("/discourse-composite/run/ruby-scripts")

for script in sorted(scripts.iterdir()):
    if script.suffix == ".rb":
        print ("Running {}".format(script))
        cmd = "cat {} | /usr/local/bin/rails c"
        code, out = subprocess.getstatusoutput(cmd.format(str(script)))
        if code != 0:
            raise ValueError("Error while running {}. Output {}".format(script, out))
