#!/usr/bin/env bash

set -e

cd /var/www/discourse

rake db:migrate

/discourse-composite/run/scripts/configure-discourse.py

# Long story short --- sometimes assets are not updated, so clean them by
# hand. I don't want to do this unconditionally as you need to restart
# service nevertheless.
# rm -rf /var/www/discourse/public/assets

rake assets:precompile

