#!/usr/bin/env bash

# Rsyslog raises some warnings which I dont like, these are related to default configurations using some
# deprecated things. Fix this follows this answer: https://serverfault.com/a/824101/3441

sudo mknod -m 640 /dev/xconsole c 1 3

sudo chown syslog:adm /dev/xconsole

sed -i '/KLogPermit/s/^/#/g' /etc/rsyslog.conf
