#!/bin/bash

cmd="$@"

/discourse-composite/run/scripts/wait-for-db.sh

set -e

if test -z ${RAILS_ENV}; then
    export RAILS_ENV=production
fi

if test ${RAILS_ENV} != production; then
    echo "Warning if RAILS_ENV is not production sidekiq will not start and your discourse won't work"

fi

# Copy envs to config
/etc/runit/1.d/copy-env

exec ${cmd}

